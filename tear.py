# This tool will open the model.ac file of a specified project and 
# replace selected texture with the new texture

#!/usr/bin/env python3

# Sample Project ID: 5d5dbd8526af9a0d78ac22a7

import os.path
import os
import re
from shutil import copy2

projectDir = './Projects/'

# Checks if project directory exists
def check_directory(projectDir, pid):
    if not os.path.isdir(projectDir + pid):
        print ("Project directory does not exist")
        quit()
    return

# Lists the discs where an annulus is present and the corresponding texture(s)
def list_textures(modelFile):
    i = 0
    for line in modelFile:
        if ".Annulus" in line:
            print (line.strip())
        if "annulus" in line:
            i += 1
            converted_num = str(i)
            print (converted_num + ") " + line)
    return

# Creates backup of original model file, checks if backup is already in place
def create_backup(projectDir, pid):     
    backedUp = os.path.exists(projectDir + pid + "/model.bak")
    if (backedUp == False):
        print("Creating backup...")
        copy2(projectDir + pid + "/model.ac", projectDir + pid + "/model.bak")
    return

# Main replacement function
def replace_textures(projectDir, pid, changeLevel, studyType):
    i = 0
    extension = ".png"
    if (studyType == "Cervical" or studyType == "Thoracic"):
        extension = ".jpg"
    
    # Anterior/Posterior/Neither definition
    while True:
        antPostInput = input("[A]nterior/[P]osterior/[N]either: ")     
        if (antPostInput == "A" or antPostInput == "a"):
            antPost = "anterior"
            break
        elif (antPostInput == "P" or antPostInput == "p"):
            antPost = "posterior"
            break
        elif (antPostInput == "N" or antPostInput == "n"):
            antPost = None
            break
        else:
            print("Invalid input")
            continue

    # Location of the tear
    while True:
        transInput = input("Location? \n [C]entral \n [R]ight \n [L]eft \n")     
        if (transInput == "c" or transInput == "C"):
            if (antPost == None):
                print("Invalid input -- Must choose left or right")
                continue
            transverse = "central"
            break
        elif (transInput == "r" or transInput == "R"):
            transverse = "right"
            break
        elif (transInput == "l" or transInput == "L"):
            transverse = "left"
            break
        else:
            print("Invalid input")
            continue

    # Some code here about the replacement

    with open(projectDir + pid + "/model.ac", "rt") as fin:
        data = fin.readlines()
        fin.close()

    with open(projectDir + pid + "/model.ac", "wt") as fout:
        print("Adjusting texture at " + changeLevel)            
        for line in data:
            if "annulus" in line:
                i += 1
                if str(i) == changeLevel and antPost == None:
                    fout.write(line.replace('annulus' + extension , 'annulus_tear_' + transverse + '.png'))
                    continue
                elif str(i) == changeLevel:
                    fout.write(line.replace('annulus' + extension , 'annulus_tear_' + antPost + '_' + transverse + '.png'))
                    continue
            fout.write(line)
        fout.close()

    # Creates lock file or will not save        
    print("Creating lockfile...\n")                 
    lockfile = open(projectDir + pid + "/lock.dat", "w")
    lockfile.write("lock.dat")
    lockfile.close()

    return

# Resets level if chosen and tear is already present -- WIP
def reset_tear(projectDir, pid, changeLevel, studyType):
    extension = ".png"
    if (studyType == "Cervical" or studyType == "Thoracic"):
        extension = ".jpg"

    with open(projectDir + pid + "/model.ac", "rt") as fin:
        data = fin.readlines()
        fin.close()

    with open(projectDir + pid + "/model.ac", "wt") as fout:
        print("Reset texture at " + changeLevel)            
        for line in data:
            if "tear" in line:
                i += 1
                if str(i) == changeLevel:
                    fout.write(line.replace("^annulus" + "$.png" , "annulus" + extension))
                    continue
            fout.write(line)
        fout.close()
    return

# Study ID input
while True:
    pid = input("Project ID: ")
    
    check_directory(projectDir, pid)

    create_backup(projectDir, pid)
    
    # All project IDs in Stark are 24 digits with the exception of dev
    if (not len(pid) == 24 and not pid=="dev"):   
        print ("Error 1: Please input a valid project ID")
        continue
    
    else: # Reads project.meta
        projectMeta = open(projectDir + pid + "/project.meta")
        print ("\n" + projectMeta.read())
        break


# Gets project study type
with open(projectDir + pid + "/project.meta") as f:
    first_line = f.readline().strip()
    studyType = first_line.split("study:",1)[1]
    f.close()


# Check supported study type
if not (studyType == "Cervical" or studyType == "Lumbar" or studyType == "Thoracic"):
    print("Unsupported study type")
    quit()


# Get level to change
while True:
    modelFile = open(projectDir + pid + "/model.ac") # Seems inefficient? Need to reopen file each read
    list_textures(modelFile)
    modelFile.close()
    changeLevel = input("Indicate number to change or [r]eset/[q]uit: ")
    if (changeLevel == "Q" or changeLevel == "q"):
        quit()
    elif(changeLevel == "R" or changeLevel == "r"):
        confirmation = input("Are you sure? (y/n) ")
        if (confirmation == "y" or confirmation == "Y"):
            print("\n\n\n****RESTORED FROM BACKUP****\n\n\n")
            copy2(projectDir + pid + "/model.bak", projectDir + pid + "/model.ac")
        continue
    else:
        replace_textures(projectDir, pid, changeLevel, studyType)
        continue
